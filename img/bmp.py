"""
bmp.py

This encoder was written in accordance to:

https://en.wikipedia.org/wiki/BMP_file_format (accessed Jan-2015)

@author: ML
"""

from containers import DefinedAttributes
from functools import wraps
import collections, struct

__UNUSED__= '\0'
__UNUSED_INT__ = 0x0

BMP_Compression = {
    'BI_RGB': 0,
    'BI_BITFIELDS': 3
}

BMP_Params_Default = (
    ('fileId','BM'),
    ('fileSize',0),
    ('pixelArrOffset',0),
    ('headerSize',0),
    ('width',0),
    ('height',0),
    ('planes',1),
    ('bitsPerPixel',32),
    ('compression',BMP_Compression['BI_RGB']),
    ('imgSize',0),
    ('resolution',(2835,2835)),   # In pixels / meter (default: 72 DPI)
    ('numPaletteColors',0),
    ('importantColors',0),
    ('redBitmask',  0x00FF0000),
    ('greenBitmask',0x0000FF00),
    ('blueBitmask', 0x000000FF),
    ('alphaBitmask',0xFF000000),
    ('colorSpaceType',0x57696E20), # default 'Win '
    ('paletteTable',()),
    
    # Internally used settings. These will be overwritten.
    ('headerLvl__',-1)
)

""" BMP headers are written to the file in order of appearance. Each 
header is represented as a tuple comprised as follows:

('Header name/version', 
    (
        ('Bit packing format', lambda f, fmt, o: f(fmt, o.id, ...)),
        ... 
    ),
    Total byte size of the header, ),
    
The lambda function splits the parameter object (o) prior to 
executing the callable (f). The callable's signature must be of
the form: func(string, ...). """
# TODO: class this
BMP_HEADERS = (
    ('BMP_FILE_HEADER', (
        ('2s', 
            lambda f,fmt,o: f(fmt, o.fileId)),
        ('<I', 
            lambda f,fmt,o: f(fmt, o.fileSize)),
        ('2s 2s I', 
            lambda f,fmt,o: f(fmt,
                __UNUSED__, 
                __UNUSED__, 
                o.pixelArrOffset)),), 14),
    ('BMP_CORE_HEADER', (
        ('3I 2H', 
            lambda f,fmt,o: f(fmt,
                o.headerSize, o.width, o.height,
                o.planes, o.bitsPerPixel)),), 12),
    ('BMP_INFO_HEADER', (
        ('6I', lambda f,fmt,o: f(fmt,
            o.compression, o.imgSize,
            o.resolution[0], o.resolution[1],
            o.numPaletteColors, o.importantColors)),), 24),
    ('BMP_V2_INFO_HEADER', (
        ('<3I', 
            lambda f,fmt,o: f(fmt,
                o.redBitmask, o.greenBitmask, o.blueBitmask)),), 16),
    ('BMP_V3_INFO_HEADER', (
        ('<I',
            lambda f,fmt,o: f(fmt,o.alphaBitmask)),), 4),
    ('BMP_V4_HEADER', (
        ('<I', 
            lambda f,fmt,o: f(fmt,o.colorSpaceType)),
        ('36s 3I', 
            lambda f,fmt,o: f(fmt,
                __UNUSED__,
                __UNUSED_INT__,__UNUSED_INT__,__UNUSED_INT__)),), 52) 
)    

def BMP_PIXEL_ENCODER(
    rawPixels, bitsPerPixel, 
    width, height, 
    padding=True, padValue=0x0, 
    emptyValue=0x0):
    '''Generator function which provides color and padding pixels.'''
    
    assert(0 < width)
    assert(0 < height)
    assert(0 < bitsPerPixel)
    assert(0 == (bitsPerPixel % 8))
    
    # BUG: how to deal w/ bits?
    bytesPerPixel = bitsPerPixel / 8
    unpaddedWidth = bytesPerPixel * width
    numPadBytes = (4 - (unpaddedWidth % 4)) % 4
    
    plen = len(rawPixels)
    
    for i in xrange(0, unpaddedWidth * height):
        
        if (i >= plen):
            yield emptyValue
        else:
            yield rawPixels[i]
        
        if ((not padding) or (0 == numPadBytes)): 
            continue
        if ((0 < i) or (1 == width)):
            # assert: no padding at index-0
            if (0 == ((i+1) % width)):
                # @ end of row: look ahead and apply padding if needed
                for j in range(0, numPadBytes):
                    yield padValue
        
def BMP_Header_Gen(headers):

    def _w_(config):
        assert(len(headers) >= config.headerLvl__)
        for name, funcs, size in headers[0 : config.headerLvl__]:
            for fmt, func in funcs:
                yield func(struct.pack, fmt, config)
    return _w_

def BMP_Pixel_Gen(pixel_gen, pixelChunkSize):

    @wraps(pixel_gen)
    def _w_(data, config):
        # crap: slow... add threading?
        assert(0 < pixelChunkSize) 
        # TODO: how to stop when external exception stops it?
        chunk = '!%dB' % pixelChunkSize 
        buf = collections.deque([], pixelChunkSize)
        
        for p in pixel_gen(
            data, 
            config.bitsPerPixel, 
            config.width, config.height):
            
            buf.append(p)
            
            if (pixelChunkSize <= len(buf)):
                yield struct.pack(chunk, *buf)
                buf.clear()
        
        if (0 < len(buf)):
            # emits left-overs, if any
            yield struct.pack('!%dB' % len(buf), *buf)
        
        buf.clear()
    
    return _w_
    
def BMP_Palette_Gen():
    
    def _w_(config):
        
        assert(config.numPaletteColors == len(config.paletteTable))
        
        if (0 == config.numPaletteColors): return
        
        yield struct.pack(
            '!%dI' % len(config.paletteTable), 
            *config.paletteTable)
            
    return _w_

def BMP_Trailer_Gen(trailers=[]):
    def _w_(config):
        for t in trailers:
            yield t
    return _w_
    
def ComposeBMPImgEncoder(head_gen, pixel_gen, palette_gen, tail_gen):
    '''A composition function which defines the file layout of the image file.
    
    Each parameter is a single byte generator for their respective section of 
    the file.'''
    # HUH?: why is a obj?
    def _w_encode(obj, data, config):
        # write headers
        for p in head_gen(config): yield p
        # now do the image        
        for p in pixel_gen(data, config): yield p
        # add the color table
        for p in palette_gen(config): yield p
        # conclude with any trailers
        for t in tail_gen(config): yield p

    return _w_encode
        
class BMPImgFileEncoder(object):
    """Encodes a 1-D integer array (0 - 255) into its BMP file equivalent.
    
    Supported Formats: 'rgb8','rgb16','rgb24','rgb32','argb32','custom'
    
    Note:
    * This version cannot accommodate images of less than 8 bpp.  
    * 'rgb8' mode requires the 'paletteTable' and numPalleteColors' be set
      appropriately.
    """
    
    __slots__ = ()
    
    def __new__(self, param_default, img_encoder, num_headers):
        '''
        :param: param_default - List of key/value pairs for setting defaults.
        :param: img_encoder - A callable defining the encoder protocol.
        :param: num_headers - Number of file headers.'''
        
        assert (0 < num_headers)
        
        self.__img_encoder__ = img_encoder
        self.__param_default__ = param_default
        # CRAP: better way to get num headers
        self.__max_headers__ = num_headers
        return object.__new__(self)

    def _map_user_settings(self, source, target):
        
        assert(isinstance(source, dict))
        
        for key in source.keys():
            if (key in target):
                setattr(target, key, source[key])

    def _override_settings(self, version, config):
        # Overrides unset or bad user settings for a file version.
        
        # init defaults
        if ('custom' != version):
            config.headerSize = 108 # VER 4 (BUG AREA)
        
        # version specific
        
        if  ('rgb1' == version):
            config.bitsPerPixel = 1
        elif ('rgb2' == version):
            config.update({
                'headerLvl__': 3,
                'headerSize': 36,
                'bitsPerPixel': 2 })
        elif ('rgb4' == version):
            config.bitsPerPixel = 4
        elif ('rgb8' == version):
            config.bitsPerPixel = 8
        elif ('rgb16' == version):
            obj = { bitsPerPixel: 16 }
            if (config.compression == BMP_Compression['BI_RGB']):
                obj.update({
                    'redBitmask':   0x7C00,
                    'greenBitmask': 0x03E0,
                    'blueBitmask':  0x001F,
                    'alphaBitmask': 0x0000 })
            config.update(obj)
        elif ('rgb24' == version):
            config.bitsPerPixel = 24
        elif ('rgb32' == version):
            config.bitsPerPixel = 32
        elif ('argb32' == version):
            config.bitsPerPixel = 32
    
        # forced defaults      
        config.paletteTable = tuple(config.paletteTable)
        config.numPaletteColors = len(config.paletteTable)
        
        if (version in ['rgb1','rgb2','rgb4','rgb8','rgb24']):
            config.compression = BMP_Compression['BI_RGB']
        
    def _set_spacings(self, config):
        
        # BUG: How to deal with bits?
        bytesPerPixel = config.bitsPerPixel / 8
        unpaddedWidth = bytesPerPixel * config.width
        paddingSize = (4 - (unpaddedWidth % 4)) % 4
        
        # (1)
        config.imgSize = (unpaddedWidth + paddingSize) * config.height
        # (2) file_hdr + DIB + color_table
        config.pixelArrOffset = 14 + config.headerSize + (4 * config.numPaletteColors)
        # (1) + (2)
        config.fileSize = config.imgSize + config.pixelArrOffset
    
    def _check_settings(self, version, config):
        
        BMP_SUPPORTED_VERS = (
            # DEBUG: 'rgb1','rgb2','rgb4',
            'rgb8','rgb16','rgb24',
            'rgb32','argb32','custom' )

        if (version not in BMP_SUPPORTED_VERS):
            raise Exception(
                """ Version '%s' in not recognized. Versions %s are 
                supported. Use 'custom' for alternative encoding 
                schemes. """ % (version, BMP_SUPPORTED_VERS))                
        
        # required palette table support
        if (version in ['rgb1','rgb2','rgb4','rgb8']):
            # TODO: need to add padding for color table 2 get to 4 byte blocks
            if (2 > len(config.paletteTable)):
                raise Exception(
                    """ The 'paletteTable' attribute is not set to  
                    a byte array of least size 2.""")
            if (2 > config.numPaletteColors):
                raise Exception(
                    """ The 'numPalleteColors' attribute is not set to  
                    at least 2.""")
        
    def encode(self, data, version, settings={}):
        '''A byte generator which encodes the 1-d integer (0 - 255) 
        data array into the specified BMP file version and settings.'''
        
        # start w/ defaults
        _config = DefinedAttributes(self.__param_default__)
        
        # Updates and overrides to file parameters
        # BUG: how to deal w/ bits?
        _config.headerLvl__ = self.__max_headers__
        self._map_user_settings(settings, _config)
        self._override_settings(version, _config)
        self._set_spacings(_config)
        self._check_settings(version, _config)
        
        for v in self.__img_encoder__.__call__(data, _config): 
            yield v
