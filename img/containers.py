"""
containers.py
"""

class DefinedAttributes(object):

    def __new__(cls_, allowed_attr):
        
        assert(len(allowed_attr) > 0)
        
        # crap: any better than sniffing?
        if (isinstance(allowed_attr[0], str)):
            allowed_kw = allowed_attr
            for name in allowed_attr:
                setattr(cls_, name, None)
        else:
            # crap: dups -- any way better?
            allowed_kw, _ = zip(*allowed_attr)
            for name, val in allowed_attr:
                setattr(cls_, name, val)
        
        cls_.__kw__ = tuple(allowed_kw)
        
        return object.__new__(cls_)
    
    def __getitem__(self, key):
        return self.__kw__[key]

    def __setattr__(self, name, value):
        if (name in self.__kw__):
            object.__setattr__(self, name, value)
            return
        # check for class-level attributes
        if (hasattr(self, name)):
            raise AttributeError(
                # TODO: get class name for printing
                """'%s' object attribute %s' is 
                read-only""" % (self.__class__.__name__, name))
            return
            
        raise AttributeError(
            """'%s' object has no defined attribute 
            '%s'""" % (self.__class__.__name__, name))

    def update(self, other):
        
        if (isinstance(other, dict)):
            for key in other:
                setattr(self, key, other[key])
            return
            
        for i in xrange(0, len(other)):
            if (isinstance(other[i], str)):
                raise ValueError(
                    """'%s' update sequence element #%d cannot be a string; 
                    it must be a key/value pair sequence.""" % (
                    self.__class__.__name__, i)
            if (len(other[i]) != 2):
                raise ValueError(
                    """'%s' update sequence element #%d has 
                    length %d; 2 is required.""" % (
                    self.__class__.__name__,i, len(other[i])))                
            setattr(self, other[i][0], other[i][1])

