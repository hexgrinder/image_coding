from bmp import *

def imageEncoder(type_, config=None):

    if ('bmp' == type_):
        return BMPImgFileEncoder(
            BMP_Params_Default,
            ComposeBMPImgEncoder(
                head_gen = BMP_Header_Gen(BMP_HEADERS),
                pixel_gen = BMP_Pixel_Gen(
                    BMP_PIXEL_ENCODER, 
                    pixelChunkSize = 32768),
                palette_gen = BMP_Palette_Gen(),
                tail_gen = BMP_Trailer_Gen()), 
            len(BMP_HEADERS))
            
