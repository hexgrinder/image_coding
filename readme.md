# BMP Image Encoder

Converts a 1-D array of integer values (0 - 255) into a BMP file.

### Supported Formats
* 'rgb8'
* 'rgb16'
* 'rgb24'
* 'rgb32'
* 'argb32'
* 'custom'
    
**Note:**

* This version does accommodate images of less than 8 bpp.  
* 'rgb8' format requires the 'paletteTable' and numPaletteColors' be set appropriately.
    
## Commonly Used Parameters

* 'width' - Image width
* 'height' - Image height

## Uncommon Parameters

* 'paletteTable' - A 1-D, 32-bit integer array. Required for 'rgb8' formats.
* 'numPaletteColors' - Number of palette colors to use in the 'paletteTable' array. Required for 'rgb8' formats.

## How to use it?
```
#!python
# Get a bitmap encoder

from img.images import imageEncoder

bmpEnc = imageEncoder("bmp")

# Set your configurations. Most times it will be the image 
# width and height.

config = {
    "width": 25,
    "height": 25 }

# Create your image file...
```

> **NOTE:** Make sure your image file matches the number of bytes per pixel 
> for the version you will select. 
>
> In the config above you have a 25x25 image for a total of 625 pixels. 
> If you select version 'rgb24', each pixel is represented by a 3-byte block. 
>
> Your final array length will be 625 * 3, or 1875.
>
> If the data array does not fill the entire image space, the encoder will 
> automatically fill and pad with 0x0 (default). 
>
> We now continue...

```
#!python
# Data file created...
image = [...] # Your 1-d integer array

# Write the data to your file. Remember to write binary!
with open('tell.bmp','wb') as outf:
    for p in bmpEnc.encode(image, 'rgb24', config):
        outf.writelines(p)
```