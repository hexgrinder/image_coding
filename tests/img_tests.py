"""
tests
"""
import sys, os, unittest, collections, struct, random, binascii

sys.path.append(os.path.abspath("../"))

from img import bmp
from img.images import imageEncoder
from img.containers import DefinedAttributes
              
class TestBMPEncoder(unittest.TestCase):

    def setUp(self):
        self.defaults = (
            ('fileId','BM'),
            ('fileSize',0),
            ('pixelArrOffset',0),
            ('headerSize',0),
            ('width',0),
            ('height',0),
            ('planes',1),
            ('bitsPerPixel',32),
            ('compression',bmp.BMP_Compression['BI_RGB']),
            ('imgSize',0),
            ('resolution',(2835,2835)),   # In pixels / meter (default: 72 DPI)
            ('numPaletteColors',0),
            ('importantColors',0),
            ('redBitmask',  0x00FF0000),
            ('greenBitmask',0x0000FF00),
            ('blueBitmask', 0x000000FF),
            ('alphaBitmask',0xFF000000),
            ('colorSpaceType',0x57696E20), # default 'Win '
            ('paletteTable',()),
            
            # Internally used settings. These are overwritten to default.
            ('headerLvl__',-1))
    
    def test_definedAttr(self):

        config = DefinedAttributes(self.defaults)

        # init check
        for key, val in self.defaults:
            self.assertEqual(getattr(config, key), val)
            
    def test_param_defaults(self):
        """ Checks default parameter values. """
        for attr, val in self.defaults:
            v = filter(lambda t: t[0] == attr, bmp.BMP_Params_Default)
            # no duplicates
            self.assertEqual(len(v), 1)
            # value is the same
            self.assertEqual(v[0][1], val)

    def test_map_user_settings(self):

        a = bmp.BMPImgFileEncoder(None, None, 5)
        config = DefinedAttributes(self.defaults)

        # write source settings
        src = {'headerSize': 1000 }
        a._map_user_settings(src, config)
        self.assertEqual(config.headerSize, 1000)

        #skip bad source settings
        src = {'__ERROR__': 1000 }
        a._map_user_settings(src, config)
        self.assertNotIn('__ERROR__', config)

    def test_override_settings(self):

        a = bmp.BMPImgFileEncoder(None, None, 5)
        config = DefinedAttributes(self.defaults)
        src = {'bitsPerPixel': 1000 }
        a._map_user_settings(src, config)

        a._override_settings('rgb2', config)
        self.assertEqual(config.bitsPerPixel, 2)
        self.assertEqual(config.headerSize, 36)

    def test_set_spacings(self):

        a = bmp.BMPImgFileEncoder(None, None, 5)
        config = DefinedAttributes(self.defaults)

        ver = 'rgb32'
        src = {
            'width': 4,
            'height': 2 }

        a._map_user_settings(src, config)
        a._override_settings(ver, config)
        a._check_settings(ver,config)
        a._set_spacings(config)

        self.assertEqual(config.bitsPerPixel, 32)
        self.assertEqual(config.pixelArrOffset, 122)
        self.assertEqual(config.headerSize, 108)
        self.assertEqual(config.imgSize, 32)
        
        ver = 'rgb24'
        src = {
            'width': 31,
            'height': 2 }

        a._map_user_settings(src, config)
        a._override_settings(ver, config)
        a._check_settings(ver,config)
        a._set_spacings(config)

        self.assertEqual(config.bitsPerPixel, 24)
        self.assertEqual(config.pixelArrOffset, 122)
        self.assertEqual(config.headerSize, 108)
        self.assertEqual(config.imgSize, 192)

        ver = 'rgb8'
        src = {
            'width': 31,
            'height': 2,
            'paletteTable': [127,10]}

        a._map_user_settings(src, config)
        a._override_settings(ver, config)
        a._check_settings(ver,config)
        a._set_spacings(config)

        self.assertEqual(config.bitsPerPixel, 8)
        self.assertEqual(config.pixelArrOffset, 130)
        self.assertEqual(config.headerSize, 108)
        self.assertEqual(config.imgSize, 64)

class TestBMPHeaders(unittest.TestCase):

    def test_bmp_headers(self):
        p = []
        # execution test
        #for name, funcs, size in bmp.BMP_HEADERS:
        #    for fmt, func in funcs:
        #        p.append(func(struct.pack, fmt, self.config))

class TestBMPPixelEncoderFunc(unittest.TestCase):

    def test_1_width(self):
        
        a = bytearray([1,2,3,4,5])
        r = []
        width = 1
        height = 3
        
        for p in bmp.BMP_PIXEL_ENCODER(a, 8, width, height, emptyValue=0x1):
            r.append(p)

        self.assertListEqual(r,
            [ 1, 0, 0, 0,
              2, 0, 0, 0,
              3, 0, 0, 0 ])

    def test_1_height(self):
        
        a = bytearray([1,2,3,4,5])
        r = []
        width = 5
        height = 1
        
        for p in bmp.BMP_PIXEL_ENCODER(a, 8, width, height, emptyValue=0x1):
            r.append(p)

        self.assertListEqual(r,
            [ 1, 2, 3, 4,
              5, 0, 0, 0 ])
        
    def test_underdim(self):
        
        a = bytearray([1,2,3,4,5])
        r = []
        width = 3
        height = 3
        
        for p in bmp.BMP_PIXEL_ENCODER(a, 8, width, height, emptyValue=0x1):
            r.append(p)

        self.assertListEqual(r,
            [ 1, 2, 3, 0,
              4, 5, 1, 0,
              1, 1, 1, 0 ])

    def test_overdim(self):
        
        a = bytearray([1,2,3,4,5,6,7,8,9,10,11,12])
        r = []
        width = 3
        height = 3
        
        for p in bmp.BMP_PIXEL_ENCODER(a, 8, width, height, emptyValue=0x1):
            r.append(p)

        self.assertListEqual(r,
            [ 1, 2, 3, 0,
              4, 5, 6, 0,
              7, 8, 9, 0 ])

    def test_argb32(self):

        a = [
            255,  0,   0, 127,   0, 255,   0, 127,
              0,  0, 255, 127, 255, 255, 255, 255 ]
        r = []
        width = 2
        height = 2
        
        for p in bmp.BMP_PIXEL_ENCODER(bytearray(a), 32, width, height):
            r.append(p)

        self.assertListEqual(r, a)

@unittest.skip("skipping")
class GeneratorTests(unittest.TestCase):

    def setUp(self):
        self.getConfig = lambda: DefinedAttributes(bmp.BMP_Params_Default)
        
    def test_pixel_gen(self):

        print '\n*** Pixel Generator Test ***\n'
        
        config = self.getConfig()
        config.update({
            'width': 5,
            'height': 2,
            'bitsPerPixel': 8
        })
        
        pixelGenerator = bmp.BMP_Pixel_Gen(bmp.BMP_PIXEL_ENCODER, 5)
        data = bytearray([1,2,3,4,5,6,7,8])
        r = ''
        print 'Generator Output:\n'
        for p in pixelGenerator(data, config):
            print binascii.hexlify(p)
            r = ''.join([r, binascii.hexlify(p)])
        
        self.assertEqual(
            r, '01020304050000000607080000000000')
        

class BMPEncoderTest(unittest.TestCase):

    def test_init(self):

        print '\n*** Encoder Test ***\n'

        bmpEnc = imageEncoder('bmp')

        config = {
            'width': 31,
            'height': 24 }
        bytesperpixel = 3
        image = []

        print 'Creating random data...\n'

        for i in xrange(0, config['width']*config['height']*bytesperpixel):
            image.append(random.randint(0,255))

        print 'Image created.\n'

        # BUG: is this writting upsidedown?
        with open('tell.bmp','wb') as outf:
            for p in bmpEnc.encode(image, 'rgb24', config):
                outf.writelines(p)
        
unittest.main()
           
