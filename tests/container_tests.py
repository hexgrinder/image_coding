"""
containers tests
"""

import sys, os, unittest, collections, struct, random, binascii

sys.path.append(os.path.abspath("../"))

from img.containers import bound_memoryview

@unittest.skip("not used -> maybe in the future?")
class BoundMemoryViewTest(unittest.TestCase):

    def setUp(self):
        
        self.data = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
        
    def test_bounds(self):
        
        split = len(self.data) / 2
        view1 = bound_memoryview(self.data, 0, split)
        view2 = bound_memoryview(self.data, split, len(self.data))
        '''
        case1 = []
        case2 = []
        
        for i in view1:
            case1.append(i)
        for i in view2:
            case2.append(i)
        '''
        self.assertListEqual(view1, self.data[0:split])
        self.assertLIstEqual(view2, self.data[split:len(self.data)])

    def test_asserts(self):
        
        with self.assertRaises(AssertionError):
            bound_memoryview(self.data, 10, 1)
        
        with self.assertRaises(AssertionError):
            bound_memoryview(self.data, 0, -1)
    
        with self.assertRaises(AssertionError):
            bound_memoryview(self.data, 0, 10000)
    
    def test_iter(self):
        
        view1 = bound_memoryview(self.data, 0, 10)
        for i in view1:
            print i

    def test_get(self):
    
        view = bound_memoryview(self.data, 1, 10)
        with self.assertRaises(IndexError):
            view[1000]
    
    def test_set(self):
    
        view = bound_memoryview(self.data, 1, 10)
        with self.assertRaises(IndexError):
            view[1000] = 2
    
    def test_len(self):
    
        view = bound_memoryview(self.data, 0, 10)
        self.assertEqual(len(view), 10)
        
class TestDefinedAttributes(unittest.TestCase):

    def test_init(self):

        # mapping check init values
        b = bmp.DefinedAttributes([('fantasy',239),('fun','fetch')])
        self.assertIn('fantasy', b)
        self.assertIn('fun', b)
        self.assertEqual(b.fantasy, 239)
        self.assertEqual(b.fun, 'fetch')

        # check empty allowed attr list
        with self.assertRaises(AssertionError):
            c = bmp.DefinedAttributes([])

        # attribute names only check init values
        c = bmp.DefinedAttributes(['fantasy','fun'])
        self.assertIn('fantasy', c)
        self.assertIn('fun', c)
        self.assertIsNone(c.fantasy)
        self.assertIsNone(c.fun)


    def test_undefined_attr(self):

        c = bmp.DefinedAttributes(['fantasy','fun'])

        # do not permit new attribute creation
        with self.assertRaises(AttributeError):
            c.fantasy1 = 2

        # non existent attribute
        self.assertNotIn('error', c)
        with self.assertRaises(AttributeError):
            print c.fantasy1

    def test_attr_assignment(self):
        
        c = bmp.DefinedAttributes(['fantasy','fun'])

        # assignment check
        c.fantasy = 4
        c.fun = '123'
        self.assertEqual(c.fantasy, 4)
        self.assertEqual(c.fun, '123')

    def test_update(self):

        c = bmp.DefinedAttributes(['fantasy','fun'])

        # update check -> dict
        c.update({ 'fantasy': 123 })
        self.assertEqual(c.fantasy, 123)
        with self.assertRaises(AttributeError):
            c.update({ 'error': 123 })
        self.assertNotIn('error', c)

        # update check -> mappings
        c.update([('fantasy', 1234)])
        self.assertEqual(c.fantasy, 1234)
        # update check -> mappings: bad sequence
        with self.assertRaises(ValueError):
            c.update(('er', 1234))
        with self.assertRaises(ValueError):
            c.update([('erdd',)])
        
        # invalid mapping
        with self.assertRaises(AttributeError):
            c.update([('error', 1234)])
        self.assertNotIn('error', c)

unittest.main()